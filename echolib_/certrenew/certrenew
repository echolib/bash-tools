#!/bin/bash
# Version 0.2.6 (2021.07.12)
# file:
# ~/echolib_/certrenew
# Description:
# Renew and get Infos from your certificates 
# It also checks and copy your certificates for your apps, like ejabberd
# Source:
# https://codeberg.org/echolib/bash-tools/raw/branch/main/echolib_/certrenew
# License:
# GPLv3 > https://codeberg.org/echolib/bash-tools/src/branch/main/LICENSE
# Part of some other bash tool:
# https://codeberg.org/echolib/bash-tools/src/branch/main/echolib_
# By echolib - https://codeberg.org/echolib/

echolib_certrenew=true
echolib_url_repo=\
"https://codeberg.org/echolib/bash-tools/raw/branch/main/echolib_"

certrenew() {
unset ${!certrenew_@}
certrenew_dir="$HOME/.config/echolib_/certrenew"
clear

# Loading modules
# ejabberd
[[ -f ~/echolib_/certrenew/certrenew-ejabberd ]] \
   && source ~/echolib_/certrenew/certrenew-ejabberd \
   || certrenew_ejabberd_noload="! No ejabberd module found"

case "$1" in

("")              certrenew__help && return ;;
help|--help|-h)   certrenew_help_opt=$2
                  [[ $certrenew_help_opt ]] \
                     && certrenew__help_opt \
                     || certrenew__help
                  return ;;

esac

! [[ `command -v certbot` ]] \
   && echo -e "! certbot package missing\n" \
   && return

certrenew_status=true
certrenew_mydir=${PWD}
! [[ -f "$certrenew_dir/certrenew.conf" ]] \
   && certrenew__create_conf \
   || source "$certrenew_dir/certrenew.conf"

case "$1" in

ejabberd|-e)   [[ $certrenew_ejabberd_noload ]] \
                  && echo "$certrenew_ejabberd_noload" \
                  || certrenew__ejabberd ;;
info|-i)       certrenew__infos ;;
conf|-c)       certrenew__conf_set ;;
reset|-r)      certrenew__create_conf ;;	
all|-a)        certbot renew --force-renewal ;;
dry|-d)        certbot --dry-run renew ;;
version|-v)    certrenew__version ;;
update|-U)     certrenew__updater ;;
*)             certrenew__help ;;

esac

cd "$certrenew_mydir"
unset ${!certrenew_@}
}

#
# LOOP Into Domains From Archive certbot Folder
#
certrenew__domains_loop() {
certrenew__status_check && return

certrenew_count=0
for certrenew_mytld in ${certrenew_tld[@]}
do
certrenew_cert_dir_tld="$certrenew_cert_arch_dir/$certrenew_mytld"
case "$1" in

check)         certrenew__domains_loop_check ;;
ejabberdcopy)  certrenew__domains_loop_ejabberdcopy ;;
createdb)      echo -e "! New Certificates found... \n"
               certrenew__domains_loop_valid ;;
checknewcert)  certrenew__domains_loop_checknewcert ;;
*)             certrenew_mainerr=true
               break ;;

esac
done
}

#
# IN LOOP. Check For fullchain & privkey certs
#
certrenew__domains_loop_check() {
certrenew__status_check && return
	
((certrenew_count++))
# Folder checking from registred Domains in conf
! [[ -d "$certrenew_cert_dir_tld" ]] \
   && echo "! No folder '$certrenew_cert_dir_tld'" \
   && certrenew_mainerr=true \
   && break

# fullchain file checking
certrenew_fullchain[$certrenew_count]=`\
ls -1 "$certrenew_cert_dir_tld" \
   | grep "fullchain" \
   | tail -1`

! [[ "${certrenew_fullchain[$certrenew_count]}" ]] \
   && echo "! No fullchain file found in $certrenew_mytld" \
   && certrenew_mainerr=true \
   && break

# privkey file checking
certrenew_privkey[$certrenew_count]=`\
ls -1 "$certrenew_cert_dir_tld" \
   | grep "privkey" \
   | tail -1`

! [[ "${certrenew_privkey[$certrenew_count]}" ]] \
   && echo "! No privkey fle found in $certrenew_mytld" \
   && certrenew_mainerr=true \
   && break
	
# checking privkey hash sha1 (comparison)
certrenew_fullchain_sha1[$certrenew_count]=`\
sha1sum "$certrenew_cert_dir_tld/${certrenew_fullchain[$certrenew_count]}" \
   | awk '{print $1}'`

certrenew_privkey_sha1[$certrenew_count]=`\
sha1sum "$certrenew_cert_dir_tld/${certrenew_privkey[$certrenew_count]}" \
   | awk '{print $1}'`
}

#
# IN LOOP. Creating Database
#
certrenew__domains_loop_valid() {
certrenew__status_check && return

certrenew_mytld=${certrenew_mytld/\//}
cd $certrenew_cert_live_dir/$certrenew_mytld
certrenew_cert_sha1=`\
sha1sum cert.pem \
   | awk '{print $1}'`

certrenew_mytld_validate=`\
certbot certificates -d "$certrenew_mytld" 2>/dev/null \
   | grep "Expiry" \
   | awk '{print $3,$4;exit}'`

certrenew_mytld_epoch=`\
date --date="$certrenew_mytld_validate" +"%s"`

echo "$certrenew_mytld $certrenew_mytld_epoch $certrenew_cert_sha1" \
	>> $certrenew_dir/certrenew.db
}

#
# IN LOOP. Check if new certs are created
#
certrenew__domains_loop_checknewcert() {
certrenew__status_check && return

cd "$certrenew_cert_live_dir/$certrenew_mytld"
certrenew_mytld=${certrenew_mytld/\//}
certrenew_cert_sha1=`\
sha1sum cert.pem \
   | awk '{print $1}'`
   
certrenew_cert_indb=`\
   grep "^$certrenew_mytld" $certrenew_dir/certrenew.db \
      | awk '{print $3}'`
      
! [[ $certrenew_cert_sha1 == $certrenew_cert_indb ]] \
	&& certrenew_reload_certs=true
}

#
# 	Check from DB (or generate new DB, if new certs) Certificates Infos
#
certrenew__infos() {
certrenew__status_check && return

! [[ -d "$certrenew_cert_live_dir" ]] \
   && echo "! certbot live folder missing" \
	&& echo "  Check and register in $certrenew_dir/certrenew.conf" \
	&& return

certrenew_tld=(${certrenew_my_domains[@]})
if	! [[ $certrenew_tld ]];then
	echo "# Checking Expiry date for all Domains in live"
	cd "$certrenew_cert_live_dir"
	certrenew_tld=(`ls -d */`)
else
	echo "# Analyse registred Domains"
	certrenew__domains_loop check
fi
[[ $certrenew_mainerr ]] \
   && return

! [[ -f $certrenew_dir/certrenew.db ]] \
	&& certrenew__domains_loop createdb

! [[ -f $certrenew_dir/certrenew.db ]] \
	&& echo "! No database found..." \
	&& return

certrenew__domains_loop checknewcert
[[ $certrenew_reload_certs ]] \
	&& rm -f "$certrenew_dir/certrenew.db" \
	&& certrenew__domains_loop createdb

certrenew__show_infos_db
}

#
#	Show from DB Certificates Infos
#
certrenew__show_infos_db() {
certrenew__status_check && return

certrenew_nowdate=`date +"%s"`
while read -r certrenew_dbline
do
certrenew_mytld=`\
echo $certrenew_dbline \
   | awk '{print $1}'`
   
certrenew_mytld_validate=`\
echo $certrenew_dbline \
   | awk '{print $2}'`

certrenew_mytld_humandate=`\
date -d @"$certrenew_mytld_validate"`

let certrenew_diffday=(\
`date +"%s" -d @"$certrenew_mytld_validate"`\
-\
`date +"%s" -d @"$certrenew_nowdate"`)\
/86400

(( $certrenew_diffday == 1 )) \
   && certrenew_showday="day" \
   || certrenew_showday="days"

echo \
"> $certrenew_mytld|\
($certrenew_diffday $certrenew_showday)|\
$certrenew_mytld_humandate" \
   >> /tmp/certrenew_tmp_certs
done < <(cat $certrenew_dir/certrenew.db)

echo
cat /tmp/certrenew_tmp_certs \
   | column -s '|' -t
rm -f /tmp/certrenew_tmp_certs
}

#
# Create Default Conf File
#
certrenew__create_conf() {
certrenew__status_check && return

mkdir -p "$certrenew_dir"
echo \
"
# certbot folders
certrenew_cert_arch_dir=\"/etc/letsencrypt/archive\"
certrenew_cert_live_dir=\"/etc/letsencrypt/live\"

# ejabberd
# ejabberd needs: DOMAIN + conference.DOMAIN + pubsub.DOMAIN + upload.DOMAIN
certrenew_ejabberd_dir=\"/etc/ejabberd\"

# Ex: (exemple.com conference.exemple.com pubsub.exemple.com upload.exemple.com)
certrenew_ejabberd_domains=()

# Check Domains
# Leave this set to scan for all domains. Set it to scan only specific ones
certrenew_my_domains=()
" >> "$certrenew_dir/certrenew.conf"
echo -e "# Configuration created in $certrenew_dir/certrenew.conf\n"
}

#
#	Reset Conf File
#
certrenew__reset_conf() {
certrenew__status_check && return

! [[ -f "$certrenew_dir/certrenew.conf" ]] \
   && return

rm -f "$certrenew_dir/certrenew.conf"
certrenew__create_conf
echo "# New Configuration File Created !"
}

#
# Edit Conf File
#
certrenew__conf_set() {
certrenew__status_check && return

! [[ `command -v nano` ]] \
   && echo "! nano missing. Use your editor to $certrenew_dir/certrenew.conf" \
   && return
	
[[ -f $certrenew_dir/certrenew.conf ]] \
   && nano $certrenew_dir/certrenew.conf \
   || echo "! No certrenew.conf file found in $certrenew_dir"
}

#
# Check if Script is launched from main name
#
certrenew__status_check() {
! [[ $certrenew_status ]] \
   && echo -e "! Use certrenew command\n"
}

# Version
certrenew__version() {
certrenew__status_check && return

echo -e \
"# certrenew: " \
`awk 'NR==2 {print $3}' \
   "$HOME/echolib_/certrenew/certrenew"`

! [[ $certrenew_ejabberd_noload ]] \
   && echo -e "# certrenew-ejabberd:" \
      `awk 'NR==2 {print $3}' \
         "$HOME/echolib_/certrenew/certrenew-ejabberd"`
}

certrenew__updater() {
certrenew__status_check && return

# source scrup udpater
! [[ -f ~/echolib_/scrup/scrup ]] \
   && echo "! No scrup updater module found" \
   && return

source ~/echolib_/scrup/scrup
scrup $echolib_url_repo/certrenew/certrenew
! [[ $certrenew_ejabberd_noload ]] \
   && scrup $echolib_url_repo/certrenew/certrenew-ejabberd
}

#
# Helps
#
certrenew__help_opt() {
case "$certrenew_help_opt" in

ejabberd)   [[ $certrenew_ejabberd_noload ]] \
               && echo "$certrenew_ejabberd_noload" \
               || certrenew__help_opt_ejabberd ;;
*)          echo -e "! No help for $certrenew_help_opt\n" ;;

esac
}

certrenew__help() {	
echo -e "# certrenew configuration file:\n  $certrenew_dir/certrenew.conf"
echo -e "\nOptions:"
echo "  help|-h [opt]		: help for [opt]"
echo "			  [opt] = ejabberd"
echo "  update | -U		: update certrenew and its modules (if available)"
echo "  conf | -c		: Configure your domains and folders (with nano)"
echo "  reset | -r		: Reset your certrenew.conf file"
echo "  all | -a		: An 'alias' to certbot renew --force-renewal"
echo "  dry | -d		: Simulate certificates renewal"
echo "  version | -v		: Show script and modules version"
echo "  ejabberd | -e		: (if needed) "
echo " 			  - Copy last fullchain & privkey certs in ejabberd folder"
echo "			  - Restart ejabberd service"
echo "  info | -i		: Checl Validity for your Domains"
echo
}
