# certrenew

Check your certs, and avoid asking certbot each time.

certbot must be installed

## Install
- Download archive, and copy echolib_ folder in your home or root session.
- In .bashrc, add the following code
```
[[ -f ~/echolib_/certrenew/certrenew ]] && . ~/echolib_/certrenew/certrenew
```

## Use
See certrenew or certrenew -h command

## Modules
- ejabberd (certrenew-ejabberd)

Check and copy your certificates - if needed - for ejabberd (XMPP).