# scrup

An updater for your scripts.

![scrup script with -U](https://codeberg.org/echolib/bash-tools/raw/branch/main/pics/echolib_scrup_-U.png "echolib scrup script with -U")

# Install
- Download archive and put echolib_ folder in your home / root
- Add this code to your .bashrc and source it
```
[[ -f ~/echolib_/scrup/scrup ]] && . ~/echolib_/scrup/scrup
```

# In your project
Do the same install, but put only scrup folder where you want and source it in .bashrc

- In scrup file, Set RAW Forge URL.
```
scrup_forge_rawurl=""
# i.e. https://codeberg.org/[ME]/[REPO]/raw/branch/main
```
(Optional)
Set A folder name where you put bar.news file
```
scrup_forge_newsdir=""
# i.e. updates
```

- In your script (i.e. bar)

At line two (2), write your version like this:
```   
# Version X.X.X
```
Where you like, add this:
```
# Source scrup file...
[[ -f ~/foobar/scrup/scrup ]] && . ~/foobar/scrup/scrup
# Your bar script [URL] in RAW !
# i.e. https://codeberg.org/[ME]/[REPO]/raw/branch/main/foo/bar
   case "$1" in
     -up) scrup [URL] ;;
   esac
```
(Optional) To show about updates for your bar script

- Create in your main forge directory, a 'news' folder named 'updates'
- In updates folder, create a simple file named bar.news
   bar.news must have the same script name, ending with .news
   (i.e. updates/bar.news
- Write in your bar.news your updates.
   They will be shown when your user wants to update

# Options
- -ht   : Explains how to use it
- -U    : Update scrup script
- [URL]	: (Set in scrup, your raw forge URL first)

# Example Use
scrup [URL] where URL is your raw URL forge script
```
bar__updater() {
! [[ -f ~/foobar/scrup/scrup ]] \
	&& echo "! No scrup updater module found" \
	&& return

source ~/foobar/scrup/scrup
scrup https://codeberg.org/ME/REPO/raw/branch/main/foobar/foo/bar
}
```