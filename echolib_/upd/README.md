# upd

*upd* is a script that automatically checks for updates for your system. It also check if reboot is needed, and in archlinux show if a new kernel is available.

It's just One or Two lines, and show nothing if there is nothing to show (no update, no kernel, no reboot)

Works with archlinux and debian (based) OS, for now.

You can see in a humain list old and new packages versions available and upgrade your system in a quick command.

## Samples

![echolib: upd](https://codeberg.org/echolib/bash-tools/raw/branch/main/pics/echolib_upd-s_upd-lc.png "echolib: upd script, doing upd -s and upd -lc")

## Options
- -s	: Show number of updates available and if reboot is needed
- -U	: Check and Update (from scrup script) upd
- -u	: Check, then Update System
- -l	: Show a list of new available updates (old/new version)
- -c	: Force checking for updates
- -t	: Show when auto check is planned
- -h	: help

## Updating System

You can edit the time in minutes between updating checks. Edit upd file and the upd_check_limit= variable

### Debian based OS

When using upd -u, You will be asked to upgrade, using dist-upgrade, full-upgrade or just upgrade (defaut when enter).

Checking for updates are Twice a Day for root user and Once a day for sudo users.

### Archlinux OS
When using upd -u, you will be asked to also upgrade kernel, if new one is available. Default (enter) doesn't update it.
 
Checking for updates are done every 2 hours as it doesn't need root (but pacman-contrib checkupdates package)

## Install
Download and extract echolib_ archive. Put echolib_ folder in you profile folder and add in .bashrc this line.

```
[[ -f ~/echolib_/upd/upd ]] && . ~/echolib_/upd/upd && upd -s
```

Optional, but recommanded, add these lines
See [scrup](https://codeberg.org/echolib/bash-tools/src/branch/main/echolib_/scrup "echolib: scrup updater for your bash scripts") (my script updater) and [prompt](https://codeberg.org/echolib/bash-tools/src/branch/main/echolib_/prompt "echolib: new prompts with history cleaner") (to have a new prompt and a better history management)

```
[[ -f ~/echolib_/functions/functions ]] && . ~/echolib_/functions/functions
[[ -f ~/echolib_/scrup/scrup ]] && . ~/echolib_/scrup/scrup
[[ -f ~/echolib_/prompt/prompt ]] && . ~/echolib_/prompt/prompt && prompt 1
```

## Want More System Stats ?

At the end of your bashrc, add this :

```
sf && upd -s
```

You will get something like this :

![echolib: sf && upd -s](https://codeberg.org/echolib/bash-tools/raw/branch/main/pics/echolib_sf_upd-s.png "echolib: Using sf && upd -s")

## TODO

I will have to better manage kernel type in archlinux (lts, zen, hardened...), as it manages only mainline one