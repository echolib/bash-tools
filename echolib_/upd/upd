#!/bin/bash
# Version 0.2.3 (2021.09.30)
# file:
# ~/echolib_/upd/upd
# Description:
# Checking for system update and get system infos
# Source:
# https://codeberg.org/echolib/bash-tools/raw/branch/main/echolib_/functions
# License:
# GPLv3 > https://codeberg.org/echolib/bash-tools/src/branch/main/LICENSE
# Part of some other bash tool:
# https://codeberg.org/echolib/bash-tools/src/branch/main/echolib_
# By echolib - https://codeberg.org/echolib/

echolib_upd=true
echolib_url_repo=\
"https://codeberg.org/echolib/bash-tools/raw/branch/main/echolib_"

if ! [[ $echolib_functions ]];then
   if [[ -f ~/echolib_/functions/functions ]];then
      . ~/echolib_/functions/functions ]]
   else
      echo "! Missing $HOME/echolib_/functions/functions"
      return
   fi
fi

upd() {
unset ${!upd_@}
unset ${!is_admin_@}
unset code_return

linuxid
# Set Automatic check time In Minutes
[[ $linux_pkg == "pacman" ]] \
   && upd_check_limit=240
if [[ $linux_pkg == "apt" ]];then
   is__admin
   [[ $is_admin_sudo ]] \
      && upd_check_limit=1440
   [[ $is_admin_root ]] \
      && upd_check_limit=770
fi

upd_arg="$1"
! [[ -f /tmp/upd_checktime ]] \
   && upd_arg="check" \
   || upd__time_analyse

case "$upd_arg" in
show|--show|-s)         upd__nbr ;;
check|--check|-c)       upd__check;upd__nbr ;;
time|--time|-t)         upd__time_show ;;
list|--list|-l)         upd__list ;;
-lc)                    upd_list_color=true;upd__list ;;
update|--update|-U)     upd__script_updater ;;
upgrade|--upgrade|-u)   upd__upgrade ;;
help|--help|-h)         upd__help ;;
=)
esac

unset ${!is_admin_@}
unset ${!upd_@}
[[ $code_return ]] \
   && return "$code_return" \
   || return 0
}

# Analysing time to check for updates
upd__time_analyse() {
[[ $upd_sysupgrading ]] \
   && return

upd_time_updated=`\
awk 'NR==1 {print $1}' /tmp/upd_checktime`

# Check Difference since last update
upd_diff_time_upd=$(( $(date +%s) - upd_time_updated ))
# Difference Converted in Minutes
upd_diff_time_min=$(( upd_diff_time_upd / 60 ))
# Check diff time from limit set
(( $upd_diff_time_min >= $upd_check_limit )) \
   && upd__check
}

# Show Next Check Update Date
upd__time_show() {
upd_time_updated=`\
awk 'NR==1 {print $1}' /tmp/upd_checktime`
upd_time_show=$(( upd_check_limit * 60 ))
upd_time_show=$(( upd_time_updated + upd_time_show ))
printf   '%b%b\n' \
         "${S_Grey} Next Check for Updates: " \
         "${C_Green}`date -d @$upd_time_show`${NC}"
}

# Check for system updates
upd__check() {
isco internet
(( $isco_online != 0 )) \
   && echo -e "${S_Red} Network: not Online" \
   && return

[[ $linux_pkg == "pacman" ]] \
   && upd__check_arch
[[ $linux_pkg == "apt" ]] \
   && upd__check_deb
}

# Check for system updates > archlinux
upd__check_arch() {
! [[ `command -v checkupdates` ]] \
   && printf   '%b%b\n' \
               "${S_Red} 'checkupdates' missing... " \
               "Install pacman-contrib" \
   && return
	
tput sc
echo -e "${S_Green} Searching for updates... (Please, wait)"
checkupdates \
            | awk '{print "-|"$1"|"$2"|"$3"|"$4}' \
            > /tmp/upd_checkupdates
code_return="$?"
upd__check_file
}

# Check for system updates > Debian (based)
upd__check_deb() {
is__admin
! [[ ${is_admin_sudo} == "sudo" || ${is_admin_root} ]] \
   && echo -e "${S_Red} Not enough rights. Cannot do this" \
   && return

tput sc
echo -e "${S_Green} Searching for updates... (Please, wait)"
[[ ${is_admin_sudo} == "sudo" ]] \
   && eval ${is_admin_sudo} -v
   
eval ${is_admin_sudo} apt-get update -qq
eval ${is_admin_sudo} apt-get -s upgrade \
	| awk -v to="->" \
         -F'[][() ]+' \
         '/^Inst/{if ($2);print "-|"$2"|"$3"|"to"|"$4}' \
	> /tmp/upd_checkupdates
code_return="$?"
unset ${!is_admin_@}
upd__check_file
}

# Delete file if not or empty
upd__check_file() {
tput rc
tput ed
[[ -f /tmp/upd_checkupdates ]] && \
(( `cat /tmp/upd_checkupdates | wc -l` == 0 )) \
   && rm /tmp/upd_checkupdates
printf "$(date +%s)" \
      > /tmp/upd_checktime
}

# Creating update message
upd__nbr() {
upd__reboot
! [[ -f /tmp/upd_checkupdates ]] \
	&& return

upd_nbr=`cat /tmp/upd_checkupdates | wc -l`
upd_show="update"
(( $upd_nbr > 1 )) && upd_show+="s"
upd_show+=" available"
[[ `cat /tmp/upd_checkupdates \
      | awk -F"|" '$2=="linux"'` ]] \
   && upd_new_krn=`\
      cat /tmp/upd_checkupdates \
         | awk -F"|" '$2=="linux" {print $5}'` \
   && upd_show+=": Kernel ${C_Yellow}$upd_new_krn${NC}"
[[ `cat /tmp/upd_checkupdates \
      | awk -F"|" '$2=="linux-headers"'` ]] \
   && upd_show+=" + headers)"
echo -e "${S_Green} $upd_nbr $upd_show"
}

# List available packages
upd__list() {
! [[ -f /tmp/upd_checkupdates ]] \
   && return

# No Color
echo -e "${S_Grey} New available Packages\n"
! [[ $upd_list_color ]] \
   && cat /tmp/upd_checkupdates | column -s '|' -t \
   && return

# With Color if ask
[[ -f /tmp/upd_checkupdates_color ]] \
   && rm /tmp/upd_checkupdates_color
   
while read -r upd_line
do 
upd_nam=`echo $upd_line | awk -F"|" '{print $2}'`
upd_old=`echo $upd_line | awk -F"|" '{print $3}'`
upd_new=`echo $upd_line | awk -F"|" '{print $5}'`
printf   "%b%b%b%b%b\n" \
         "${C_Grey}-${NC}|" \
         "${C_Cyan}$upd_nam${NC}|" \
         "${C_Yellow}$upd_old${NC}|" \
         "${C_Grey}->${NC}|" \
         "${C_Green}$upd_new${NC}" \
      >> /tmp/upd_checkupdates_color
done < <(cat /tmp/upd_checkupdates)

cat /tmp/upd_checkupdates_color \
   | column -s '|' -t
}

# Check if reboot needed, analysing kernel for archlinux
upd__reboot() {
[[ $linux_pkg == "pacman" ]] && upd__reboot_arch
[[ $linux_pkg == "apt" ]] && upd__reboot_deb
}

# Archlinux reboot check from kernel
upd__reboot_arch() {
upd_krn_cur=`uname -r`

upd_boot_krn=`\
cat /proc/cmdline \
   | grep "BOOT_IMAGE" \
   | awk -F 'boot' '{print $2;exit}' \
   | awk '{print $1}'`

if [[ $upd_boot_krn ]];then # Sometimes, is empty
   upd_krn_new=`\
   file /boot$upd_boot_krn \
      | awk '{for(i=1; i<=NF; i++) \
            if($i~/version/) print $(i+1)}'`
   [[ $upd_krn_cur != $upd_krn_new ]] \
      && printf \
         "%b%b%b\n" \
         "${S_Red} ${C_Green}$upd_krn_cur${NC} " \
         "-> ${C_Yellow}$upd_krn_new${NC} " \
         "${C_Grey}(${NC}${C_Red}Reboot${NC}${C_Grey})${NC}" \
      && code_return="3"
fi
}

upd__reboot_deb() {
[[ -f /var/run/reboot-required ]] \
   && echo -e "${C_Red}# Reboot Needed !${NC}" \
   && code_return="3"
}

# source scrup udpater
upd__script_updater() {
! [[ -f ~/echolib_/scrup/scrup ]] \
   && echo "! No scrup updater module found" \
   && return

source ~/echolib_/scrup/scrup
scrup "$echolib_url_repo/upd/upd"
}

upd__upgrade() {
is__admin
! [[ ${is_admin_sudo} == "sudo" || ${is_admin_root} ]] \
   && echo -e "${S_Red} Not enough rights. Cannot do this" \
   && return

[[ $linux_pkg == "pacman" ]] \
   && upd__upgrade_arch
[[ $linux_pkg == "apt" ]] \
   && upd__upgrade_deb
(( "$code_return" == 0 )) && \
[[ -f /tmp/upd_checkupdates ]] \
   && rm /tmp/upd_checkupdates

unset ${!is_admin_@}
}

# Upgrade System from Debian
upd__upgrade_arch() {
[[ -f /var/lib/pacman/db.lck ]] \
   && echo -e "${S_Yellow} System is upgrading... or pacman is locked" \
   && return
	
upd__check_arch
! [[ -f /tmp/upd_checkupdates ]] \
   && echo -e "${S_Green} No updates available" \
   && return

[[ ${is_admin_sudo} == "sudo" ]] \
   && eval ${is_admin_sudo} -v
eval ${is_admin_sudo} pacman -Sy &>/dev/null

clear
upd__nbr
upd_pkg_upg="--ignore=linux --ignore=linux-headers"
if [[ $upd_new_krn ]];then
	read -rp \
   "`printf \
      '%b%b%b' \
      "${S_Yellow} Upgrade Kernel: " \
      "${C_Red}C${NC}ancel | ${C_Yellow}Y${NC}es | ${C_Green}*${NC} " \
      "(Default No) ? ${C_Yellow}" \
   `" upd_arch_ask
   tput sgr0
   
	case "$upd_arch_ask" in
	C|c)  code_return="1";return ;;
	Y|y)  unset upd_pkg_upg ;;
	*)    true ;;
	esac
fi

echo -e "${S_Yellow} Upgrading System...\n"
eval ${is_admin_sudo} pacman -Su --needed $upd_pkg_upg
code_return="$?"
} 

# Upgrade System from Debian
upd__upgrade_deb() {
[[ `ps aux | grep [a]pt` ]] \
   && echo -e \
      "${S_Yellow} System is upgrading... or apt /archives is in use" \
   && return
[[ `ps aux | grep [d]pkg` ]] \
   && echo -e \
      "${S_Yellow} System is upgrading... or dpkg is in use" \
   && return

upd__check_deb
! [[ -f /tmp/upd_checkupdates ]] \
   && echo -e "${S_Green} No updates available" \
   && return

clear
is__admin
read -rp \
"`printf \
         '%b%b%b%b' \
         "${S_Yellow} ${is_admin_sudo} Upgrade Perform: " \
         "${C_Red}C${NC}ancel | ${C_Yellow}F${NC}ull " \
         "| ${C_Red}D${NC}ist | ${C_Green}U${NC}grade " \
         "(Default: U) ? ${C_Yellow}" \
`" upd_deb_ask
tput sgr0
   
case "$upd_deb_ask" in
C|c)     code_return="1";return ;;
D|d)     upd_deb_upg="dist-upgrade" ;;
F|f)     upd_deb_upg="full-upgrade" ;;
U|u|*)   upd_deb_upg="upgrade" ;;
esac

echo -e "${S_Yellow} Upgrading System...\n"
eval ${is_admin_sudo} apt-get $upd_deb_upg
code_return="$?"
}

upd__help() {
cat << EOF
$ upd [ARG]: Check and show system updates, upgrade
  -u       : UPGRADE System
  -h       : This help
  -s       : Show if new updates and new kernel
  -U       : Update this upd script
  -c       : Check for System updates, and show them
  -t       : Show when next check for updates will happen
  -l       : Show available packaqges (no color)
  -lc      : Same as -l but colored (slower)
EOF
}
