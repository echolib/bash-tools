# prompt

Test and add new Prompts in your bash profile. These new prompts include No duplicated commands, and refresh history across multiple terminal.

## Install

To try and register these new prompts, add in your profile
```
[[ -f ~/echolib_/prompt/prompt ]] && . ~/echolib_/prompt/prompt && prompt 1
```

## Use

Use 'prompt -h' for Help

Type 'prompt' command to test them

type *prompt set* to Register One in your profile