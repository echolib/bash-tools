# functions

Functions used by other echolib_ script, but you can use them in your scripts.

Type echolib for help and echolib -s to show functions.

With 'sf' command, you will see current System Statistics from disks, memory, cpu, network, packages... There is also a weather line from wttr.

Functions available are at least :
echolib | dsk | pmu | tsk | wth | cpul | dlup | usb | isco ...

See pics folder to show samples of scripts

# install
To install these scripts, just download the archive and extract echolib_ folder in your profile session folder.

To activate them, according your needs, add in .bashrc file at the end (for instance)

```
[[ -f ~/echolib_/functions/functions ]] && . ~/echolib_/functions/functions
[[ -f ~/echolib_/scrup/scrup ]] && . ~/echolib_/scrup/scrup
[[ -f ~/echolib_/upd/upd ]] && . ~/echolib_/upd/upd && upd -s
[[ -f ~/echolib_/prompt/prompt ]] && . ~/echolib_/prompt/prompt && prompt 1
```

You could put only upd line, these scripts will source them when needed.

# Scripts

- upd: A system updater (check, show, upgrade...)
- functions: Many functions
- scrup: An "universal" updater for your scripts
- prompt: new prompts including history cleaning 
- certrenew: Show Certificates Expirations and avoid asking certbot each time
- - Module: certrenew-ejabberd : quickly update your certificates

# Sample
![sf + tsk commands](https://codeberg.org/echolib/bash-tools/raw/branch/main/pics/echolib_sf_tsk-0.3.png "sf function and tsk commands")