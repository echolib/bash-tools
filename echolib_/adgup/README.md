# adgup

adgup is a simple updater for AdGuardHome.

Sometimes, from UI, you cannot update. This script will update for you, replacing executable file, like mentionned in the official process.

Before updating, it will download the archive, check if the executable version is different, and then, make a backup of the original file, renaming it as .last in /opt/AdGuardHome, and copy the new one.

It will also stop the service before updating (if it is active), and will restart it after, if it was active.

Always inform you about the service status.

## Note

The default downloaded version is the armv6 (32 bits) recommended for the RaspberryPi. You can change it, edting adgup file.

Replace archive name value:

adgup_tar_vers="AdGuardHome_linux_armv6.tar.gz"

with yours from URL here:

https://github.com/AdguardTeam/AdGuardHome/wiki/Getting-Started#installation

## Install

Put in your profile bash:

```
[[ -f ~/echolib_/adgup/adgup ]] && . ~/echolib_/adgup/adgup
```

```
adgup
```

## update

Make sure, you extracted echolib_ folder from this repo, first.
```
adgup -U
```