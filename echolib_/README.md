# echolib_ bash-tools

Tools written in bash. Should help getting a better life from Terminal.

- functions: Many functions, some used in echolib_ scripts. 

Type echolib for help and echolib -s to see them

- scrup: An "universal" updater for your scripts
- prompt: new prompts including history cleaning 
- certrenew: Show Certificates Expirations and avoid asking certbot each time
- - Module: certrenew-ejabberd : quickly update your certificates