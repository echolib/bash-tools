# echolib_ bash-tools

Tools written in bash. Should help getting a better life from Terminal.

# install
To install these scripts, just download the archive and extract echolib_ folder in your profile session folder.

To activate them, according your needs, add in .bashrc file at the end (for instance)

```
[[ -f ~/echolib_/functions/functions ]] && . ~/echolib_/functions/functions
[[ -f ~/echolib_/scrup/scrup ]] && . ~/echolib_/scrup/scrup
[[ -f ~/echolib_/upd/upd ]] && . ~/echolib_/upd/upd && upd -s
[[ -f ~/echolib_/prompt/prompt ]] && . ~/echolib_/prompt/prompt && prompt 1
```

You could put only upd line, these scripts will source them when needed.

# Scripts

- upd: A system updater (check, show, upgrade...)
- functions: Many functions.

Type echolib for help and echolib -s to show functions. 

- scrup: An "universal" updater for your scripts
- prompt: new prompts including history cleaning 
- certrenew: Show Certificates Expirations and avoid asking certbot each time
- - Module: certrenew-ejabberd : quickly update your certificates